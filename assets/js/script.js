/**********************************************************
 *                                                        *
 * ----------------------Navigation-----------------------*
 *                                                        *
 **********************************************************/

//Cibler les boutons de Navigation

let $presentationButton = document.querySelector('#presentationButton');
let $parcoursButton = document.querySelector('#parcoursButton');
let $technosButton = document.querySelector('#technosButton');
let $passionsButton = document.querySelector('#passionsButton');

//Cibler les sections

let $presentationSection = document.querySelector('#presentationSection');
let $parcoursSection = document.querySelector('#parcoursSection');
let $technosSection = document.querySelector('#technosSection');
let $passionsSection = document.querySelector('#passionsSection');

//Cibler les li de la barre de Navigation
let $presentationLi = document.querySelector('#presentationLi');
let $parcoursLi = document.querySelector('#parcoursLi');
let $technosLi = document.querySelector('#technosLi');
let $passionsLi = document.querySelector('#passionsLi');

//On cible les éléments qui changeront au click de la navbar
let $titleSection = document.querySelector('#titleSection');
let $imgAvatar = document.querySelector('#imgAvatar');
let $bgHeader = document.querySelector('#bgHeader');

//On donne à ces éléments une valeur par défaut
$titleSection.innerHTML = "Mais qui suis-je ? Et que fais-je ?";
$imgAvatar.style.backgroundImage = "url('./assets/img/bgAvatar1.png')";
$bgHeader.style.backgroundImage = "linear-gradient(45deg, rgb(255, 166, 0), rgb(241, 21, 21))"

//Place des écouteurs d'évènements sur le click des boutons de Navigation

//Pour la présentation
$presentationButton.addEventListener('click', function (e) {
   e.preventDefault();
   //On retire la classe disable et liDisable de la section concernée
   $presentationSection.classList.remove('disable');
   $presentationLi.classList.remove('liDisable');
   //On ajoute la classe disable et liDisable aux autres
   $parcoursSection.classList.add('disable');
   $parcoursLi.classList.add('liDisable');
   $technosSection.classList.add('disable');
   $technosLi.classList.add('liDisable');
   $passionsSection.classList.add('disable');
   $passionsLi.classList.add('liDisable');
   //On change la valeur des éléments du header
   $titleSection.innerHTML = "Mais qui suis-je ? Et que fais-je ?";
   $imgAvatar.style.backgroundImage = "url('./assets/img/bgAvatar1.png')";
   $bgHeader.style.backgroundImage = "linear-gradient(45deg, rgb(255, 166, 0), rgb(241, 21, 21))"

});

//Pour les compétences
$parcoursButton.addEventListener('click', function (e) {
   e.preventDefault();
   //On retire la classe disable de la section concernée
   $parcoursSection.classList.remove('disable');
   $parcoursLi.classList.remove('liDisable');
   //On ajoute la classe disable aux autres
   $presentationSection.classList.add('disable');
   $presentationLi.classList.add('liDisable');
   $technosSection.classList.add('disable');
   $technosLi.classList.add('liDisable');
   $passionsSection.classList.add('disable');
   $passionsLi.classList.add('liDisable');
   //On change la valeur des éléments du header
   $titleSection.innerHTML = "Mon parcours !";
   $imgAvatar.style.backgroundImage = "url('./assets/img/bgAvatar2.png')";
   $bgHeader.style.backgroundImage = "linear-gradient(45deg, rgb(255, 134, 219),rgb(112, 17, 121))"

});

//Pour les technos et logiciels
$technosButton.addEventListener('click', function (e) {
   e.preventDefault();
   //On retire la classe disable de la section concernée
   $technosSection.classList.remove('disable');
   $technosLi.classList.remove('liDisable');
   //On ajoute la classe disable aux autres
   $presentationSection.classList.add('disable');
   $presentationLi.classList.add('liDisable');
   $parcoursSection.classList.add('disable');
   $parcoursLi.classList.add('liDisable');
   $passionsSection.classList.add('disable');
   $passionsLi.classList.add('liDisable');
   //On change la valeur des éléments du header
   $titleSection.innerHTML = "Que sais-je utiliser ?";
   $imgAvatar.style.backgroundImage = "url('./assets/img/bgAvatar3.png')";
   $bgHeader.style.backgroundImage = "linear-gradient(45deg, rgb(138, 172, 245),rgb(17, 41, 121))"

});

//Pour les passions
$passionsButton.addEventListener('click', function (e) {
   e.preventDefault();
   //On retire la classe disable de la section concernée
   $passionsSection.classList.remove('disable');
   $passionsLi.classList.remove('liDisable');
   //On ajoute la classe disable aux autres
   $presentationSection.classList.add('disable');
   $presentationLi.classList.add('liDisable');
   $parcoursSection.classList.add('disable');
   $parcoursLi.classList.add('liDisable');
   $technosSection.classList.add('disable');
   $technosLi.classList.add('liDisable');
   //On change la valeur des éléments du header
   $titleSection.innerHTML = "Et sinon j'aime quoi ?";
   $imgAvatar.style.backgroundImage = "url('./assets/img/bgAvatar4.png')";
   $bgHeader.style.backgroundImage = "linear-gradient(45deg, rgb(138, 245, 170),rgb(65, 160, 10))"

});

/**********************************************************
 *                                                        *
 * -------------------Fin-Navigation----------------------*
 *                                                        *
 **********************************************************/

/*********************************************************
*                                                        *
* --------------------Menu-Contacts----------------------*
*                                                        *
**********************************************************/

//Cibler le bouton qui enclenchera l'évènements

let $contactsButton = document.querySelector('#menuContactButton');

//Cibler la div contenant le bouton

let $divContactsButton = document.querySelector('#contactsButton');

//Cibler la div qui sera en déplacement

let $listContacts = document.querySelector('#listContacts');

//Placer un écouteur d'évènement sur le click du bouton

$contactsButton.addEventListener('click', function (e) {
   e.preventDefault();
   //Ajouter une classe si elle ne l'a pas, ou la retirer si elle l'a
   $listContacts.classList.toggle('translateX');
   $divContactsButton.classList.toggle('translateX');
});

/*********************************************************
*                                                        *
* ------------------Fin-Menu-Contacts--------------------*
*                                                        *
**********************************************************/
/*********************************************************
*                                                        *
* -----------------------Passions------------------------*
*                                                        *
**********************************************************/

//Cibler les boutons qui ramènent vers la navigation des passions

let $toNavigation1 = document.querySelector('#toNavigation1');
let $toNavigation2 = document.querySelector('#toNavigation2');
let $toNavigation3 = document.querySelector('#toNavigation3');
let $toNavigation4 = document.querySelector('#toNavigation4');

//Cibler les boutons dans la navigation

let $buttonMondeFafy = document.querySelector('#buttonMondeFafy');
let $buttonJeux = document.querySelector('#buttonJeux');
let $buttonFossiles = document.querySelector('#buttonFossiles');

//Cibler les sections 

let $ecranTitre = document.querySelector('#ecranTitre');
let $ecranNavigation = document.querySelector('#ecranNavigation');
let $ecranMondeFafy = document.querySelector('#ecranMondeFafy');
let $ecranJeux = document.querySelector('#ecranJeux');
let $ecranFossiles = document.querySelector('#ecranFossiles');
//Créer les écouteurs d'évènement pour chaque click sur les boutons

//Pour se rendre à la navigation

//On cible les zones de texte de chaque section

let $textMondeFafy = document.querySelector('#textMondeFafy');
let $textJeux = document.querySelector('#textJeux');
let $textFossiles = document.querySelector('#textFossiles');

//On va créer une fonction pour pas répéter le code

function toNavigation() {
   //On enlève la classe disable à la navigation
   $ecranNavigation.classList.remove('disable');
   //On l'ajoute aux autres sections
   $ecranMondeFafy.classList.add('disable');
   $ecranJeux.classList.add('disable');
   $ecranTitre.classList.add('disable');
   $ecranFossiles.classList.add('disable');

   //On vide toutes les zones de textes de chaque section de leur contenue
   $textMondeFafy.innerHTML =' ';
   $textJeux.innerHTML =' ';
   $textFossiles.innerHTML=' ';
}

$toNavigation1.addEventListener('click', function (e) {
   e.preventDefault();
   toNavigation();
});
$toNavigation2.addEventListener('click', function (e) {
   e.preventDefault();
   toNavigation();
});
$toNavigation3.addEventListener('click', function (e) {
   e.preventDefault();
   toNavigation();
});
$toNavigation4.addEventListener('click', function (e) {
   e.preventDefault();
   toNavigation();
});


//On crée les tableaux de textes concernant les différentes parties

let arrayTextMondeFafy = [
   'Le monde de Fafy est la page facebook sur laquelle je poste des dessins, des strip, ou parfois des hommages. <a href="#" id="buttonSuite">Suite</a>',
   'Je dessine depuis que je sais tenir un crayon, sur feuille blanche, dans la marge de mes cahiers, (sur le mur blanc chez mes parents, oups !). <a href="#" id="buttonSuite">Suite</a>',
   'J\'ai un style plutôt cartoonesque, comme on peut le voir sur ce PortFolio. Ce que je préfère, c\'est dessiner des créatures ou des personnages Chibi ! <a href="#" id="buttonSuite">Suite</a>',
   'Le dessin, c\'est quelque chose que je n\'arrêterai jamais !'
];

let arrayTextJeux = [
   'Aah les jeux vidéos, j\'ai baigné dedans toute mon enfance, et ça me suit encore aujourd\'hui ! Ce que j\'adore, c\'est de pouvoir me déplacer librement. Les RPG à monde ouvert sont mon style de jeux préféré ! <a href="#" id="buttonSuite">Suite</a>',
   'Je joue d\'une façon particulière, je ne me mets à fond sur un jeu que lorsque j\'ai fini à 100% le dernier jeu sur lequel j\'étais à fond. Autant dire que je suis sur Skyrim depuis 2011, et ça ne me lasse pas ! <a href="#" id="buttonSuite">Suite</a>',
   'J\'aime bien jouer de façon role play, me mettre dans la peau du personnage, les jeux infinis du type Minecraft me permettent d\'être une sorcière capitaliste, chose un peu difficile en vrai !'
];

let arrayTextFossiles = [
   'Les fossiles et minéraux, j\'en ai des cartons entiers, quelque part dans hangar chez mes parents, je les récupère durant mes expéditions, ou auprès d\'une dame qui voulait s\'en débarrasser ! <a href="#" id="buttonSuite">Suite</a>',
   'Dès que je fais une sortie, je regarde toujours par terre en quête d\'un éventuel fossile, si bien que j\ai fini par contaminer mon conjoint qui fait maintenant de même ! <a href="#" id="buttonSuite">Suite</a>',
   'Cette passion pour les fossiles en particulier, je la tiens de mon amour pour les dinosaures ! Plus jeune, je voulais être paléontologue, mais le trop peu de débouchés m\'a décidé à me réorienter ! <a href="#" id="buttonSuite">Suite</a>',
   'J\'en ai emporté une poignée avec moi, et petit à petit je ramènerai le reste pour tout exposer en vitrine !'
];

//On crée la fonction pour afficher le texte dans la div et le faire passer au suivant
function textPassions(div, array, i) {
   let iterator = i+1;
   div.innerHTML = array[iterator];

   //On cible le nouveau lien "suite"
   let buttonSuite = document.querySelector('#buttonSuite');
   buttonSuite.addEventListener('click', function(e){
      e.preventDefault();
      textPassions(div, array, iterator);
   });
}

//Pour se rendre dans les autres sections

$buttonMondeFafy.addEventListener('click', function (e) {
   e.preventDefault();
   //On enlève la classe disable à cette section
   $ecranMondeFafy.classList.remove('disable');
   //on l'ajoute aux autres sections
   $ecranFossiles.classList.add('disable');
   $ecranNavigation.classList.add('disable');
   $ecranTitre.classList.add('disable');
   $ecranJeux.classList.add('disable');

   //On s'occupe d'afficher le bon texte dans la bonne div !
   $textMondeFafy.innerHTML = arrayTextMondeFafy[0];

   //On cible le lien "suite"
   let buttonSuite = document.querySelector('#buttonSuite');

   //On appelle la fonction pour faire passer le message sur le clique du lien Suite
   buttonSuite.addEventListener('click', function (e) {
      e.preventDefault();
      textPassions($textMondeFafy, arrayTextMondeFafy, 0);
   })
});

$buttonJeux.addEventListener('click', function (e) {
   e.preventDefault();
   //On enlève la classe disable à cette section
   $ecranJeux.classList.remove('disable');
   //on l'ajoute aux autres sections
   $ecranFossiles.classList.add('disable');
   $ecranNavigation.classList.add('disable');
   $ecranTitre.classList.add('disable');
   $ecranMondeFafy.classList.add('disable');


   //On s'occupe d'afficher le bon texte dans la bonne div !
   $textJeux.innerHTML = arrayTextJeux[0];

   //On cible le lien "suite"
   let buttonSuite = document.querySelector('#buttonSuite');

   //On appelle la fonction pour faire passer le message sur le clique du lien Suite
   buttonSuite.addEventListener('click', function (e) {
      e.preventDefault();
      textPassions($textJeux, arrayTextJeux, 0);
   })
});

$buttonFossiles.addEventListener('click', function (e) {
   e.preventDefault();
   //On enlève la classe disable à cette section
   $ecranFossiles.classList.remove('disable');
   //on l'ajoute aux autres sections
   $ecranMondeFafy.classList.add('disable');
   $ecranNavigation.classList.add('disable');
   $ecranTitre.classList.add('disable');
   $ecranJeux.classList.add('disable');


   //On s'occupe d'afficher le bon texte dans la bonne div !
   $textFossiles.innerHTML = arrayTextFossiles[0];

   //On cible le lien "suite"
   let buttonSuite = document.querySelector('#buttonSuite');

   //On appelle la fonction pour faire passer le message sur le clique du lien Suite
   buttonSuite.addEventListener('click', function (e) {
      e.preventDefault();
      textPassions($textFossiles, arrayTextFossiles, 0);
   })
});